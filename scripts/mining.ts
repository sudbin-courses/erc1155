import { moveBlocks } from "../utils/move-blocks"

async function mining() {
	await moveBlocks(1)
}

mining()
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error)
		process.exit(1)
	})
