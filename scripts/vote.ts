import * as fs from "fs"
import { network, ethers } from "hardhat"
import { PROPOSAL_ID, VOTING_DELAY } from "../hardhat.config"
import { moveBlocks } from "../utils/move-blocks"
import { voteStatus } from "../utils/vote-status"

async function main() {

  console.log(`\n\n-----------------------------------------`)


  console.log(`Proposed with proposal ID:  ${PROPOSAL_ID}`)
  /**
   * Очень важно решение по голосованию
   * Типизация: 0 = Against, 1 = For, 2 = Abstain for this example
   *  ! Если будет не набрано решение с положительными голосами - оно будет неуспешным.
   *  ! Но также оно может быть не успешным из-за того что закончилось по времени
   */
  const voteWay = 1
  const reason = "I lika do da cha cha"
  await vote( voteWay, reason)
  console.log(`-----------------------------------------\n\n`)
}

export async function vote(voteWay: number, reason: string) {
  console.log("Voting...")

  /**
   *   const signers = await ethers.getSigners()

  signers.forEach(signer => console.log(`${signer.address}`))
   */
  const [signer] = await ethers.getSigners()
  
  const governor = await ethers.getContract("GovernorContract", signer);

  if (governor.hasVoted(signer)) {
    console.log('User already voted: ' + signer.address)
    return false
  }
  
  const proposalState = voteStatus(await governor.state(PROPOSAL_ID))

  if(proposalState == 'Pending' || proposalState == 'Active' || proposalState == 'Queued'){
    const voteTx = await governor.castVoteWithReason(PROPOSAL_ID, voteWay, reason)

    const voteTxReceipt = await voteTx.wait(1)
    console.log(voteTxReceipt.events[0].args.reason)
    

    if (process.env.NODE_ENV === 'local') {
      await moveBlocks(VOTING_DELAY + 1)
    }

    console.log("Voting success")
  } else {
    console.log("Voting canceled")
    console.log(`Current Proposal State: ${proposalState}`)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
