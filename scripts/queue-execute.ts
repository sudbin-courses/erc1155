import { ethers, network } from "hardhat"
import {
  MIN_DELAY,
} from "../hardhat.config"
import { moveBlocks } from "../utils/move-blocks"
import { moveTime } from "../utils/move-time"

export async function queueAndExecute() {

  console.log(`\n\n\n-----------------------------------------`)

  const args = [process.env.PROJECT_ID]
  const functionToCall = process.env.FUNCTION || ''

  const box = await ethers.getContract("Box")
  const encodedFunctionCall = box.interface.encodeFunctionData(functionToCall, args)
  const descriptionHash = ethers.utils.keccak256(ethers.utils.toUtf8Bytes(process.env.DESCRIPTION || ''))
  // could also use ethers.utils.id(description)

  const [signer] = await ethers.getSigners()
  const governor = await ethers.getContract("GovernorContract", signer);

  console.log("Queueing...")
  const queueTx = await governor.queue(
    [box.address], 
    [0], 
    [encodedFunctionCall], 
    descriptionHash
  )
  await queueTx.wait(1)

  if (process.env.NODE_ENV === 'local') {
    await moveTime(MIN_DELAY + 1)
    await moveBlocks(1)
  }

  console.log("Executing...")
  // this will fail on a testnet because you need to wait for the MIN_DELAY!
  const executeTx = await governor.execute(
    [box.address],
    [0],
    [encodedFunctionCall],
    descriptionHash
  )
  await executeTx.wait(1)
  console.log(`Box value: ${await box.retrieve()}`)

  console.log(`-----------------------------------------\n\n\n`)
}

queueAndExecute()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
