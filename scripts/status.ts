import { ethers } from "hardhat"
import { PROPOSAL_ID } from "../hardhat.config"
import { voteStatus } from "../utils/vote-status"

async function main() {
	console.log(`\n\nProposal Id: ${PROPOSAL_ID}`)

	const governor = await ethers.getContract("GovernorContract")
	const proposalState = await governor.state(PROPOSAL_ID)

	console.log(`Status: ${voteStatus(proposalState)}\n\n`)
}

main()
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error)
		process.exit(1)
	})
