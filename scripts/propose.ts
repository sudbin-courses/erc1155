import { ethers } from "hardhat"
import {
	DESCRIPTION,
	FUNCTION,
	PROJECT_ID,
  VOTING_DELAY,
} from "../hardhat.config"
import { moveBlocks } from "../utils/move-blocks"

export async function propose(args: any[], functionToCall: string, desc: string) {

    const [signer] = await ethers.getSigners()
	console.log('\nSigner addres: ' + signer.address)
	
	const box = await ethers.getContract("Box")
	const governor = await ethers.getContract("GovernorContract", signer);
	
    const encodedFunctionCall = box.interface.encodeFunctionData(functionToCall, args)
    console.log(`Proposing ${functionToCall} on ${box.address} with project id: ${args}`)
    console.log(`Proposal Description:  ${desc}`)
    const proposeTx = await governor.propose(
      [box.address],
      [0],
      [encodedFunctionCall],
      desc
    )

    if (process.env.NODE_ENV === 'local') {
      await moveBlocks(VOTING_DELAY + 1)
    }

    const proposeReceipt = await proposeTx.wait(1)
    const proposalId = proposeReceipt.events[0].args.proposalId
    console.log(`Proposed with proposal ID:  ${proposalId}`)


    console.log(`-----------------------------------------\n\n\n`)
}

propose([PROJECT_ID], FUNCTION || '', DESCRIPTION || '')
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
