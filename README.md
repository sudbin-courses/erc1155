<h1 align="center">This is an example of an <a href="https://t.me/danilsudbin" target="_blank">DAO</a></h1>

[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Fira+Code&duration=2000&pause=1000&lines=DAO;Access+level;Hardhat;Ethers;Binance+smart+chain)](https://git.io/typing-svg)

## Getting Started

1. В данном репозитории рассматривается полный цикл взаимодействия со стандартом erc1155
    - [Access level](./docs/access_control.md)
2. Документация [hardhat](https://hardhat.org/hardhat-runner/docs/config)
3. Документация [Open zeppelin](https://docs.openzeppelin.com/contracts/4.x/governance)

## Installation

1. `yarn`
2. `cp .env.example .env` - далее необходимо настроить свои ключи
3. `npx hardhat compile` -> `hh compile` need install globally `hardhat-shorthand`
    - Команда установки: `yarn global add hardhat-shorthand`.
    - Теперь можно использовать синтаксис: `hh {commandName}`

## Debug

1. `yarn clean` - сбросит порт для ноды и очистит проект
2. `hh node` - соберет смарт-контракты в папку `/artifacts` и задеплоит их на локальную ноду
2. `hh deploy` - выполнит скрипты из папки deploy (скрипты для публикации смарт-контрактов в сети)
3. Возможные ошибки:
    - Обязательно проверить все переменные `.env`, особенно `WALLET_PRIVATE_KEY` и `NODE_ENV`
    - Если подвисает нода: поменять rpc url - `https://chainlist.org/`
4. Скрипты для отладки:
    - Во всех скриптах для отладки необходимо указывать сеть
    - `hh run scripts/propose.ts --network localhost` - публикация предложения для голосования

Result example:

```
GovernanceToken: 0x821A2f5ecb9de476C8d725e9c50756Bb942Dc837 (verified)
TimeLock: 0xC4bD9a230181deFeBe5C5cb5Fb628bC77a79dcAa (not verified)
GovernorContract: 0xbAcF1fF5bA8E528acA48C38AB76594995e77c9A3 (not verified)

Propose ID: 60712197226662762106547168189278476395971719761690017178676747052392212468563
```



## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)