import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"
import "dotenv/config"
import { QUORUM_PERCENTAGE, VOTING_DELAY, VOTING_PERIOD } from "../hardhat.config"

/**
 * Деплой контракта с реализацией DAO
 * 
 * Настрока:
 *  - ERC20 токен который нужен при голосовании
 *  - Контракт который блокирует DAO после сбора голосов
 * 
 * Установили значения:
 *  - Кворум
 *  - Период голосования
 *  - Задержка после сбора всех голосов
 */
const deployGovernor: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {

  console.log('Deploy Governor smart-contract...')

  const { getNamedAccounts, deployments } = hre
  const { deploy, get } = deployments
  const { deployer } = await getNamedAccounts()

  // Дополнительный экспорт для связки двух контрактов
  const governanceToken = await get("GovernanceToken")
  const timeLock = await get("TimeLock")

  const smartContract = await deploy("GovernorContract", {
    from: deployer,
    args: [
      governanceToken.address,
      timeLock.address,
      QUORUM_PERCENTAGE,
      VOTING_PERIOD,
      VOTING_DELAY,
    ],
    log: true,
    waitConfirmations: 1,
  })

  console.log('QUORUM_PERCENTAGE: ' + QUORUM_PERCENTAGE)
  console.log('VOTING_PERIOD: ' + VOTING_PERIOD)
  console.log('VOTING_DELAY: ' + VOTING_DELAY)

  if (process.env.NODE_ENV != 'local') {
    await verify(smartContract.address, [])
  } else {
    console.log('Verification is not needed');
  }

  console.log('\n\n')
}

export default deployGovernor
deployGovernor.tags = ["all", "governor"]