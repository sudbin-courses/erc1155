import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import "dotenv/config"
import { ethers } from "hardhat"

/**
 * Распределение доступов
 */
const setup: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {

    console.log('Setting up DAO roles...')

    const { getNamedAccounts } = hre
    const { deployer } = await getNamedAccounts()

    const timeLock = await ethers.getContract("TimeLock", deployer)
    const governor = await ethers.getContract("GovernorContract", deployer)

    /**
     * Кто может выносить предложение на голосование
     * Выдаем доступ: участник смарт-контракта
     */
    const proposerRole = await timeLock.PROPOSER_ROLE()
    const proposerTx = await timeLock.grantRole(proposerRole, governor.address)
    await proposerTx.wait(1)

    /**
     * Кто может окончить голосование и применить результаты
     * Выдаем доступ: владелец смарт-контракта
     */
    const executorRole = await timeLock.EXECUTOR_ROLE()
    const executorTx = await timeLock.grantRole(executorRole, deployer )
    await executorTx.wait(1)

    /**
     * Кто может заблокировать на время выполнение контракта
     * Выдаем доступ: владелец смарт-контракта
     */
    const adminRole = await timeLock.TIMELOCK_ADMIN_ROLE()
    const revokeTx = await timeLock.revokeRole(adminRole, deployer)
    await revokeTx.wait(1)

    console.log('Setup finished')
    console.log('\n\n')
}

export default setup
setup.tags = ["all", "setup"]