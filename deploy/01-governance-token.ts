import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"
import "dotenv/config"
import { ethers } from "hardhat"

/**
 * Деплой контракта с реализацией ERC20Votes стандарта
 */
const deployGovernanceToken: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {

  console.log('\n\n')
  console.log('Deploy GovernanceToken smart-contract...')

  const { getNamedAccounts, deployments } = hre
  const { deploy } = deployments
  const { deployer } = await getNamedAccounts()
  
  console.log('deployer address: ' + deployer)

  const smartContract = await deploy("GovernanceToken", {
    from: deployer,
    args: [],
    log: true,
    waitConfirmations: 1,
  })

  if (process.env.NODE_ENV != 'local') {
    await verify(smartContract.address, [])
  } else {
    console.log('Verification is not needed');
  }

  await delegate(smartContract.address, deployer)

  console.log('\n\n')
}

const delegate = async (governanceTokenAddress: string, delegatedAccount: string) => {
  console.log('\n\n')
  console.log(`Delegating to ${delegatedAccount}`)
  const governanceToken = await ethers.getContractAt("GovernanceToken", governanceTokenAddress)
  const transactionResponse = await governanceToken.delegate(delegatedAccount)
  await transactionResponse.wait(1)
  console.log("Delegated!")
}

export default deployGovernanceToken
deployGovernanceToken.tags = ["all", "governance"]