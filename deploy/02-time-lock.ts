import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"
import "dotenv/config"
import { MIN_DELAY } from "../hardhat.config"

/**
 * Деплой контракта с реализацией ERC20Votes стандарта
 */
const deployTimeLock: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {

  console.log('Deploy TimeLock smart-contract...')

  const { getNamedAccounts, deployments } = hre
  const { deploy } = deployments
  const { deployer } = await getNamedAccounts()

  const smartContract = await deploy("TimeLock", {
    from: deployer,
    args: [MIN_DELAY,[],[]],
    log: true,
    waitConfirmations: 1,
  })

  if (process.env.NODE_ENV != 'local') {
    await verify(smartContract.address, [])
  } else {
    console.log('Verification is not needed');
  }

  console.log('\n\n')
}

export default deployTimeLock
deployTimeLock.tags = ["all", "timelock"]