import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"
import "dotenv/config"
import { ethers } from "hardhat"

/**
 * Деплой контракта с реализацией ERC20Votes стандарта
 */
const deployBox: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {

    console.log('Deploy Box smart-contract...')

    const { getNamedAccounts, deployments } = hre
    const { deploy } = deployments
    const { deployer } = await getNamedAccounts()
    
    console.log('deployer address: '+deployer)

    const smartContract = await deploy("Box", {
        from: deployer,
        args: [],
        log: true,
        waitConfirmations: 1,
    })

    if (process.env.NODE_ENV != 'local') {
        await verify(smartContract.address, [])
    } else {
        console.log('Verification is not needed');
    }

    console.log('\n\n')
}

export default deployBox
deployBox.tags = ["all", "box"]