import { network } from "hardhat"

export async function moveBlocks(amount: number) {
  console.log("Mining 1 block...")
  for (let index = 0; index < amount; index++) {
    await network.provider.request({
      method: "evm_mine",
      params: [],
    })
  }
  console.log(`Mining finish`)
}
