

export function voteStatus( id:number ) {
    switch (id) {
        case 0:
            return "Pending";   // В ожидании
        case 1:
            return "Active";    // Активный
        case 2:
            return "Canceled";  // Отменено
        case 3:
            return "Defeated";  // Побежденный
        case 4:
            return "Succeeded"; // Успешно
        case 5:
            return "Queued";    // В очереди
        case 6:
            return "Expired";   // Истек
        case 7:
            return "Executed";  // Выполнен
    }
}