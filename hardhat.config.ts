import "@typechain/hardhat"
import "@nomiclabs/hardhat-etherscan"
import "@nomiclabs/hardhat-ethers"
import "dotenv/config"
import "hardhat-deploy"
import { HardhatUserConfig } from "hardhat/config"

const config: HardhatUserConfig = {
    defaultNetwork: "hardhat",
    networks: {
		hardhat: {
			chainId: 31337,
			allowUnlimitedContractSize: true,
		},
		localhost: {
			chainId: 31337,
			allowUnlimitedContractSize: true,
		},
		binance: {
			url: process.env.BINANCE_NODE_URL,
			accounts: [process.env.WALLET_PRIVATE_KEY || ''],
			chainId: 97,
		}
    },
    solidity: {
		version: "0.8.9",
		settings: {
			optimizer: {
				enabled: true,
				runs: 200,
			},
		},
    },
    etherscan: {
      	apiKey: process.env.SCAN_API_KEY,
    },
    namedAccounts: {
		deployer: {
			default: 0,
			1: 0,
		},
    },
}

export default config

// Константы для Governor контракта

/**
 * Сколько блоков надо подождать чтобы голосование стало активным
 * Cкорость их появления зависит от блокчейна
 */
export const VOTING_DELAY = 0
export const MIN_DELAY = 0 // Время блокировки для TimeLock контракта
export const QUORUM_PERCENTAGE = 1
export const VOTING_PERIOD = 48 // Кол-во блоков

/**
 * Переменные для теста DAO
 */
export const PROJECT_ID = 1
export const FUNCTION = 'store'
export const DESCRIPTION = 'Propose: 1'
export const PROPOSAL_ID = '60712197226662762106547168189278476395971719761690017178676747052392212468563'